/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

import com.mycompany.consoleio.ConsoleIO;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PIN {

    private int pin;

    private ConsoleIO IO;

    public void setPIN(String newPIN) {

        
        PrintWriter out = null;
        
        try {
            out = new PrintWriter(new FileWriter("PIN.txt"));

            out.print(newPIN);

            out.flush();

        } catch (IOException ex) {

        } finally {
            out.close();

        }

    }

    public PIN() {

        pin = decode();

    }

    public int getPinNum() {
        return pin;
    }

    public void setPinNum(int pin) {
        this.pin = pin;

        encode();
    }

    public void encode() {

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter("PIN.txt"));

            out.print(pin);

            out.flush();

        } catch (IOException ex) {

        } finally {
            out.close();

        }

    }

    public int decode() {

        int savingBalance = 0;

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("PIN.txt")));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                savingBalance = Integer.parseInt(currentLine);

            }
        } catch (FileNotFoundException ex) {

        }

        return savingBalance;
    }
}
