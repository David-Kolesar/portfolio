/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

/**
 *
 * @author apprentice
 *
 */
import com.mycompany.consoleio.ConsoleIO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Calendar;
import java.util.TimerTask;

public class Savings extends Account {

    private double checkBalance = 0;
    private double interest = 0;
    private ConsoleIO IO;
    private double bigDeposit = 0;
    private double preBalance = 0;

    public Savings() {

        this.balance = decode();

    }

    @Override
    public void withdraw(double withdraw) {

        if (withdraw > balance) {
            System.out.println("Insufficient funds for withdrawal.");
        }
        if (withdraw > checkBalance + bigDeposit) {

            System.out.println("This amount of funds must be validated by a bank manager before eligible withdrawal.");

            checkBalance = 0; // This makes it so that the manager has cleared the withdrawal. Put this on a timer if you want to be high speed.
        } else {

            balance -= (withdraw += .10);

            System.out.println("Since you are withdrawing from your Savings Account, there is a 10 cent penalty. ");

            encode();
        }
    }

    @Override
    public void deposit(double deposit) {

        preBalance = balance;

        if (deposit > 10000) {

            checkBalance = preBalance;

            System.out.println("Since you have deposited more than $10,000, a manager must clear the deposit before you are allowed to withdraw any of this money.");

            double bigDeposit = deposit;

        }

        balance += deposit;

        encode();

    }

    public double viewBalance() {

        return balance;

    }

    public void encode() {

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter("SavingsAccount.txt"));

            out.print(balance += interest);

            interest = 0;

            out.flush();

        } catch (IOException ex) {

        } finally {
            out.close();

        }

    }

    public double decode() {

        double savingBalance = 0;

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("SavingsAccount.txt")));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                savingBalance = Double.parseDouble(currentLine);

            }
        } catch (FileNotFoundException ex) {

        }

        return savingBalance;
    }

    public class ReportGenerator extends TimerTask {

        public void run() {
            System.out.println("Generating report");
            //TODO generate report
        }

    }

    public void addInterest() {

        interest = (balance * .10);
    }

    public void interestTime() {

        Calendar today = Calendar.getInstance();
        Calendar interestDay = Calendar.getInstance();
        interestDay.set(2016, 9, 07);
        if (today.equals(interestDay)) {
            addInterest();
        }

    }

    public void harassManagerSavings() {

        System.out.println("You call the manager on his cell phone (conveniently written on the ATM) and badger him to authorize your withdrawals. He does so begrudgingly.");

        checkBalance = 0; // This makes it so that the manager has cleared the withdrawal. Put this on a timer if you want to be high speed.
        bigDeposit = 0;
    }

}
