/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

/**
 *
 * @author David Kolesar
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Checking extends Account {

    private double bigDeposit = 0;
    private double preBalance = 0;
    private double checkBalance = 0;

    public Checking() {
        this.balance = decode();

    }

    @Override
    public void withdraw(double actualDeduction) {

        
         if (actualDeduction > checkBalance+bigDeposit) {
            
             System.out.println("This amount of funds must be validated by a bank manager before eligible withdrawal.");
        
            
         }
        
        if (actualDeduction < balance ) {
           
            balance -= actualDeduction;

            encode();
            
        } 
        
        if (actualDeduction < balance + 100)  {

            double negative = actualDeduction -= balance;

           balance = negative * -1;
            
                       System.out.println("Overdraft protection has been implemented. Please deposit money within three business days to avoid overdraft fees.");
            
            encode();

        }
        if (actualDeduction > balance + 100)  {
        
            System.out.println("Your overdraft protection may not exceed $100. You have insufficient funds for this withdrawal.");
        }

    }

    @Override
    public void deposit(double deposit) {

        if (deposit > 10000) {

            checkBalance = preBalance;

            System.out.println("Since you have deposited more than $10,000, a manager must clear the deposit before you are allowed to withdraw any of this money.");

            double bigDeposit = deposit;

        }

        balance += deposit;

        encode();
    }

    public double viewBalance() {

        return balance;
    }

    public void encode() {

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter("CheckingAccount.txt"));

            out.print(balance);

            out.flush();

        } catch (IOException ex) {

        } finally {
            out.close();

        }

    }

    public double decode() {

        double savingBalance = 0;

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("CheckingAccount.txt")));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                savingBalance = Double.parseDouble(currentLine);

            }
        } catch (FileNotFoundException ex) {

        }

        return savingBalance;
    }
    
    public void harassManagerChecking(){
        
        System.out.println("You call the manager on his cell phone (conveniently written on the ATM) and badger him to authorize your withdrawals. He does so begrudgingly.");
        
         checkBalance = 0; // This makes it so that the manager has cleared the withdrawal. Put this on a timer if you want to be high speed.
         bigDeposit = 0;
    }

}
