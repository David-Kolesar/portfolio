/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;

import Account.Checking;
import Account.PIN;
import Account.Savings;
import com.mycompany.consoleio.ConsoleIO;

/**
 *
 * @author apprentice
 */
public class BankController {

    public Savings savings = new Savings();

    private ConsoleIO IO;

    private Checking checking = new Checking();

    private PIN pin = new PIN();

    public BankController(ConsoleIO IO) {

        this.IO = IO;

    }

    public void run() {

        Savings interest = new Savings();
        
        interest.interestTime();
        
        boolean valid = true;

        boolean accept = true;

        while (accept) {

            String inputString = IO.getString("Please Enter your PIN number.");

            int counter = inputString.length();

            int inputInt = Integer.parseInt(inputString);

            if (counter < 4 || counter > 4) {

                IO.displayString("A valid PIN must contain 4 characters");

            }

            if (inputInt == pin.getPinNum()) {
                accept = false;
            } else {
                System.out.println("PIN is incorrect. Please Reenter your PIN.");
            }

        }

        while (valid) {
            System.out.println("Select an account \n 1. Savings \n 2. Checking \n 3. Set PIN \n 4. Exit");
            int selection = IO.getIntRange("", 4, 1);

            switch (selection) {

                case 1:
                    savingsMenu();
                    break;
                case 2:
                    checkingMenu();
                    break;
                case 3:
                    createPIN();
                    break;
                case 4:
                    System.exit(0);

            }
        }

    }

    private void savingsMenu() {

        boolean valid = true;

        while (valid) {
            System.out.println("Select an option \n 1. View Balance \n 2. Withdraw \n 3. Deposit \n 4. Harass the bank manager \n 5. Exit");
            int selection = IO.getIntRange("", 5, 1);

            switch (selection) {

                case 1:
                    double balance = savings.viewBalance();
                    IO.displayString("Your Savings Account balance is $" + balance);
                    break;
                case 2:
                    double withdraw = IO.getDouble("How much would you like to withdraw?");
                    savings.withdraw(withdraw);
                    break;
                case 3:
                    double deposit = IO.getDouble("How much would you like to deposit?");
                    savings.deposit(deposit);
                    break;
                case 4:
                    harassManagerS();
                    break;
                case 5:
                    System.exit(0);
                    break;

            }

        }

    }

    private void checkingMenu() {

        boolean valid = true;

        while (valid) {
            System.out.println("Select an option \n 1. View Balance \n 2. Withdraw \n 3. Deposit \n 4. harass Manager \n 5. Exit");
            int selection = IO.getIntRange("", 5, 1);

            switch (selection) {

                case 1:
                    double balance = checking.viewBalance();
                    IO.displayString("Your Checking Account balance is $" + balance);
                    break;
                case 2:
                    double withdraw = IO.getDouble("How much would you like to withdraw?");
                    checking.withdraw(withdraw);
                    break;
                case 3:
                    double deposit = IO.getDouble("How much would you like to deposit?");
                    checking.deposit(deposit);
                    break;
                case 4:
                    harassManagerC();
                    break;
                case 5:
                    System.exit(0);

            }

        }
    }

    public void createPIN() {

        IO.displayString("You may now set your pin. \n This is a four digit number which allows you to have access to your account.");

        int firstNumber = IO.getIntRange("Please enter the first number in your PIN", 9, 0);
        int secondNumber = IO.getIntRange("Please enter the second number in your PIN", 9, 0);
        int thirdNumber = IO.getIntRange("Please enter the third number in your PIN", 9, 0);
        int fourthNumber = IO.getIntRange("Please enter the fourth number in your PIN", 9, 0);

        String firstString = Integer.toString(firstNumber);
        String secondString = Integer.toString(secondNumber);
        String thirdString = Integer.toString(thirdNumber);
        String fourthString = Integer.toString(fourthNumber);

        String newPIN = firstString + secondString + thirdString + fourthString;

        
        pin.setPIN(newPIN);
    }
    
    public void harassManagerS(){
        savings.harassManagerSavings();
    }

    
    public void harassManagerC(){
        checking.harassManagerChecking();
    }
}



