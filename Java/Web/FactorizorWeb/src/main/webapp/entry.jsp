<%-- 
    Document   : entry
    Created on : Sep 9, 2016, 4:41:18 PM
    Author     : David Kolesar
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>RSVP To Party</h1>
    
        Can you attend?
        
        <form action="RSVPServlet" method="POST">
            Yes <input type="checkbox" value="y" name="myResponse" /><br />
            No <input type="checkbox" value="n" name="myResponse" /><br />
            <input type="submit" value="Answer" />
            
            
            
        </form>
    </body>
</html>
