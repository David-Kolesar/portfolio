<%-- 
    Document   : theresponse
    Created on : Sep 9, 2016, 4:00:32 PM
    Author     : David Kolesar
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Results</title>

        <style>

            #error {
                color: red;
            }

        </style>

    </head>
    <body>

        <h1>Results</h1>

        <div id="error" >
            ${error} <br />
        </div>

        The proper factors of ${numberToFactor} are ${factors}. <br />

        (Total Number of Proper Factors: ${numberOfFactors}) <br /> <br />
        ${numberToFactor} is ${isPrime} a prime number. <br />
        ${numberToFactor} is ${isPerfect} a perfect number. <br />

    </body>
</html>