/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.factorizorweb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author David Kolesar
 */
@WebServlet(name = "FactorizorServlet", urlPatterns = {"/FactorizorServlet"})
public class FactorizorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String numberToFactorString = request.getParameter("numberToFactor");

        Integer numberToFactor;

        try {
            numberToFactor = Integer.parseInt(numberToFactorString);
        } catch (NumberFormatException ex) {
            numberToFactor = null;
        }

        try {

            int numberOfFactors = calculateNumberOfFactors(numberToFactor);
            boolean numberToFactorIsPerfect = isPerfect(numberToFactor);
            boolean numberToFactorIsPrime = isPrime(numberToFactor);
            List factorList = getFactors(numberToFactor);

            request.setAttribute("numberToFactor", numberToFactor);
            request.setAttribute("factors", factorList);
            request.setAttribute("numberOfFactors", numberOfFactors);

            if (numberToFactorIsPerfect) {
                request.setAttribute("isPerfect", "");
            } else {
                request.setAttribute("isPerfect", "not");
            }

            if (numberToFactorIsPrime) {
                request.setAttribute("isPrime", "");
            } else {
                request.setAttribute("isPrime", "not");
            }

        } catch (NullPointerException ex) {
            request.setAttribute("error", "You must input a positive counting number");
        }

        RequestDispatcher rd = request.getRequestDispatcher("response.jsp");
        rd.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public List getFactors(int numberToFactor) {

        List factorList = new ArrayList();

        for (int i = 1; i < numberToFactor; i++) {
            if (numberToFactor % i == 0) {
                factorList.add(i);
            }
        }

        return factorList;

    }

    public boolean isPrime(int numberToCheck) {

        boolean numberToCheckIsPrime = true;

        if (numberToCheck == 1) {
            numberToCheckIsPrime = false;
        }

        for (int i = 2; i < numberToCheck; i++) {
            if (numberToCheck % i == 0) {
                // i is not a prime number
                numberToCheckIsPrime = false;
            }
        }

        return numberToCheckIsPrime;

    }

    public boolean isPerfect(int numberToCheck) {

        boolean numberToCheckIsPerfect = false;
        int sumOfFactors = 0;

        for (int i = 1; i < numberToCheck; i++) {
            if (numberToCheck % i == 0) {
                sumOfFactors += i;
            }
        }

        if (sumOfFactors == numberToCheck) {
            numberToCheckIsPerfect = true;
        }

        return numberToCheckIsPerfect;

    }

    public int calculateNumberOfFactors(int numberToCheck) {

        int numberOfFactors = 0;

        for (int i = 1; i < numberToCheck; i++) {
            if (numberToCheck % i == 0) {
                numberOfFactors++;
            }
        }

        return numberOfFactors;

    }

}