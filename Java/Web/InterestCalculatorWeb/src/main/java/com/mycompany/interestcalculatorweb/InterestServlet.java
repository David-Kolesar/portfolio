/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interestcalculatorweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "InterestServlet", urlPatterns = {"/InterestServlet"})
public class InterestServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("response.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String capitalString = request.getParameter("startCapital");            //gets user input for capital
        Double capitalDouble;

        try {                                                                   //validates input with try/catch method
            capitalDouble = Double.parseDouble(capitalString);
        } catch (NumberFormatException ex) {
            capitalDouble = null;
        }

        String interestString = request.getParameter("startInterest");          //gets user input for interest
        Double interestDouble;

        try {                                                                   //validates input with try/catch method
            interestDouble = Double.parseDouble(interestString);
        } catch (NumberFormatException ex) {
            interestDouble = null;
        }

        String yearsString = request.getParameter("years");                     //gets user input for capital
        Double yearsDouble = null;

        try {                                                                   //validates input with try/catch method
            yearsDouble = Double.parseDouble(yearsString);
        } catch (NumberFormatException ex) {
            yearsDouble = null;
        }

        try {                                                                   //validates results with try/catch method

            List[] resultsList = runMe(yearsDouble, interestDouble, capitalDouble);

            List<Double> yearsList = resultsList[0];
            List<Double> interestList = resultsList[1];
            List<Double> totalList = resultsList[2];

            request.setAttribute("yearsList", yearsList);
            request.setAttribute("interestList", interestList);
            request.setAttribute("totalList", totalList);

        } catch (NullPointerException ex) {
            request.setAttribute("error", "Your bet must be a dollar amount.");
        }

        RequestDispatcher rd = request.getRequestDispatcher("resultsTable.jsp");//Defines an object that receives requests from the client and sends them to any resource
        rd.forward(request, response);                                          //forwards to request, response

    }

    @Override
    public String getServletInfo() {                                            //Gives info about servlet
        return "logic for Interest Calculator";
    }// </editor-fold>

    public List[] runMe(double userCapital, double userInterest, double years) {

        List<Double> yearsList = new ArrayList<Double>();
        List<Double> interestList = new ArrayList<Double>();
        List<Double> totalList = new ArrayList<Double>();

        List[] resultsList = {yearsList, interestList, totalList};

        double compoundCapital = userCapital;
        
        for (double i = 0; i < years; i++) {

            double x = compoundCapital;    
            
            yearsList.add(i+1);

            Double amountOfInterest = compoundCapital * userInterest;

            interestList.add(amountOfInterest);
            
            totalList.add(compoundCapital = (compoundCapital + amountOfInterest));
            
        }
        return resultsList;
    } // make an object that has three lists and return that object

}
