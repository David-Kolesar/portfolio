<%-- 
    Document   : resultsTable
    Created on : Sep 11, 2016, 3:11:53 PM
    Author     : apprentice
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Results Table</title>
    </head>
    <body>
        <h1>Results Table</h1>

        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }

        </style>
    </head>
<body>

    <table>
        <tr>
            <th>Years Invested</th>
            <th>Interest Accrued</th>
            <th>Total Amount</th>
        </tr>
        <tr> 
            <td><c:forEach var="year" items="${yearsList}">                        <!--instead of looping over three lists, find a variable that represents each year - printing out each year printing out table cells we need. -->
                    <fmt:formatNumber type="number" value="${year}" />  <br/>      <!--Refers to var established in previous line-->
                </c:forEach></td>
            <td>
                <c:forEach var="interest" items="${interestList}">
                     <fmt:formatNumber type="currency" value="${interest}" />  <br/>  
                </c:forEach>
            </td>
            <td>
                <c:forEach var="total" items="${totalList}">
                     <fmt:formatNumber type="currency" value="${total}" />  <br/>   
                </c:forEach>
            </td>
        </tr>

    </table>





 
</body>
</html>
