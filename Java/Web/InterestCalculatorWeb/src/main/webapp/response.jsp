<%-- 
    Document   : response
    Created on : Sep 11, 2016, 2:48:08 PM
    Author     : apprentice
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index</title>
        <style>

            h1, p {
                text-align: center;
            }

        </style>
    </head>
    <body>
        <h1>Interest Calculator</h1>
        <form action="InterestServlet" method="POST">
            <p> Please indicate your initial investment : <input type="text" value="0.00" name="startCapital"/> </p>
            <p> Please indicate the yearly interest rate of the investment : <input type="text" value="0.00" name="startInterest"/> </p>
            <p> Please indicate how many years you plan on saving  :<input type="text" value="0.00" name="years"/> </p>
            <p> <input type="submit" value="Calculate" /> </p>
        </form>
    </body>
</html>