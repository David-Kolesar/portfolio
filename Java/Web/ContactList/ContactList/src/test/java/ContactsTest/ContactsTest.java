///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package ContactsTest;
//
//import com.mycompany.ContactList.dao.ContactDao;
//import com.mycompany.ContactList.dto.Contact;
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
///**
// *
// * @author apprentice
// */
//public class ContactsTest {
//
//    ContactDao dao;
//    Contact dto;
//    Contact newContact = new Contact();
//
//    public ContactsTest() {
//
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
//
//        dao = ctx.getBean("ContactDao", ContactDao.class);
//
//    }
//
//    private static void clearData() {
//
//        File xx = new File("testFile.txt");
//        if (xx.exists()) {
//            xx.delete();
//            try {
//                xx.createNewFile();
//            } catch (IOException ex) {
//                Logger.getLogger(ContactsTest.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
//
//    @BeforeClass
//    public static void setUpClass() { //This only happens before the test is run
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() { //This happens after the test is complete
//    
//    //blow away anything current in file
//    
//    //write 1 with id 1
//    
//    
//    }
//
//    @After
//    public void tearDown() { //this happens after every test has ran
//    }
//
//    @Test
//    public void add() {
//
//        
//        //get all 
//        
//        //count entries
//        
//        //expected id count + 1
//        
//        
//        
//        int expectedSize = 1;
//        Integer expectedId = 1;  //Arrange
//        
//
//        newContact.setCompany("company");
//        newContact.setEmail("email");
//        newContact.setFirstName("First");
//        newContact.setLastName("Last");
//        newContact.setPhone("22");
//        
//        
//        
//        newContact = dao.create(newContact); // Act
//
//        Assert.assertEquals(newContact.getId(), expectedId); //Assert
//        Assert.assertEquals(expectedSize, dao.all().size());
//
//    }
//    
//    @Test
//    public void all() {
//        int expected = 1;
//        int actual = dao.all().size();
//
//        Assert.assertEquals(expected, actual);
//    }
//
//    
//    @Test
//    public void delete() {
//
//        int expectedSum = 0;
//        dao.delete(newContact);
//        int actual = dao.all().size();
//
//        Assert.assertEquals(actual, expectedSum);
//
//    }
//
//}
//
//
////
////public class ContactsTest {
////
////    ContactDao dao;
////    Contact dto;
////    Contact newContact = new Contact();
////    List<Contact> backupList = dao.all();
////
////    public ContactsTest() {
////
////        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-persistence.xml");
////
////        dao = ctx.getBean("ContactDao", ContactDao.class);
////
////    }
////
////    private static void clearData() {
////
////        File xx = new File("testFile.txt");
////        if (xx.exists()) {
////            xx.delete();
////            try {
////                xx.createNewFile();
////            } catch (IOException ex) {
////                Logger.getLogger(ContactsTest.class.getName()).log(Level.SEVERE, null, ex);
////            }
////        }
////    }
////
////    @BeforeClass
////    public static void setUpClass() { //This only happens before the test is run
////    
////    
////    
////    }
////
////    @AfterClass
////    public static void tearDownClass() {
////    }
////
////    @Before
////    public void setUp() { //This happens after the test is complete
////
////        
////    }
////
////    @After
////    public void tearDown() { //this happens after every test has ran
////
////        List<Contact> testList = backupList;
////
////        for (Contact c : testList) {
////            dao.delete(c);
////        }
////
////        for (Contact c : backupList) {
////            dao.create(c);
////
////        }
////
////        //repopulate list
////    }
////
////    @Test
////    public void add() {
////
////        List<Contact> testList = backupList;
////
////        int expectedListSize = testList.size() + 1;
////
////        //get all 
////        //count entries
////        //expected id count + 1
////        Integer expectedId = 1;  //Arrange
////
////        newContact.setCompany("company");
////        newContact.setEmail("email");
////        newContact.setFirstName("First");
////        newContact.setLastName("Last");
////        newContact.setPhone("22");
////
////        newContact = dao.create(newContact); // Act
////
////        Assert.assertEquals(newContact.getId(), expectedId); //Assert
////        Assert.assertEquals(expectedListSize, dao.all().size());
////
////    }
////
////    @Test
////    public void all() {
////        int expected = 1;
////        int actual = dao.all().size();
////
////        Assert.assertEquals(expected, actual);
////    }
////
////    @Test
////    public void delete() {
////
////        int expectedSum = 0;
////        dao.delete(newContact);
////        int actual = dao.all().size();
////
////        Assert.assertEquals(actual, expectedSum);
////
////    }
////
////}
