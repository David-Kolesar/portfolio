<%-- 
    Document   : show
    Created on : Sep 14, 2016, 10:22:57 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Showing Contact</h1>
        
      First Name: ${contact.firstName} <br />
      Last Name: ${contact.lastName} <br />
      Company: ${contact.company} <br />
      Email: ${contact.email} <br />
      Phone: ${contact.phone} <br />
     
    </body>
</html>
