<%-- 
    Document   : home
    Created on : Sep 13, 2016, 2:44:20 PM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Company Contacts</title>

        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
              rel="stylesheet">

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">


        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }
        </style>


    </head>
    <body>
        <div class="container">

            <jsp:include page="Navbar.jsp"></jsp:include>



                <div class="row">
                    <div class="col-sm-6" >
                        <p><h1>My Contacts</h1></p>


                        <table class ="table table-responsive">

                            <tr>
                                <th>Name</th>
                                <th>Company</th>
                                <th></th>
                                <th></th>
                            </tr>



                        <c:forEach items="${contactList}" var="contact">

                            <tr>
                                <td></td>
                            </tr>

                            <tr>
                                <td><a href = "${pageContext.request.contextPath}/contact/show/${contact.id}"> ${contact.firstName} ${contact.lastName}</td>    
                                <td>${contact.company}</td>
                                <td><a href="${pageContext.request.contextPath}/contact/edit/${contact.id}">Edit</a></td>
                                <td><a href="${pageContext.request.contextPath}/contact/delete/${contact.id}">Delete</a></td>
                            <tr>
                            <tr>

                            <tr>
                            </c:forEach>

                    </table>   

                </div>
                <div class="col-sm-6" >
                    <div class="form-horizontal">


                        <div class="row">
                            <div class="text-left"><h2>Add New Contact</h2></div>
                        </div>

                        
                        <form style="padding-left: 80px"method="POST" action="contact/create">
                            <div class="form-group">
                                <label>First Name:</label>
                                <input class="form-control" type="text" name="firstName" />
                            </div>
                            <div class="form-group">
                                <label>Last Name:</label>
                                <input class="form-control" type="text" name="lastName" />
                            </div>
                            <div class="form-group">
                                <label>Company:</label>
                                <input class="form-control" type="text" name="company" />
                            </div>
                            <div class="form-group">
                                <label>Email:</label>
                                <input class="form-control" type="text" name="email" />
                            </div>
                            <div class="form-group">
                                <label>Phone:</label>
                                <input class="form-control" type="text" name="phone" />
                            </div>
                            <input class ="btn" type="submit" value="Save" /> 
                        </form>
                    </div>
                </div>
            </div>
            <div class="span5">
            </div>

            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
