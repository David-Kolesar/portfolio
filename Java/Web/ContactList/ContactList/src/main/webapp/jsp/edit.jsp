<%-- 
    Document   : edit
    Created on : Sep 14, 2016, 9:24:09 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Edit Contact</h1>

        <form action ="${pageContext.request.contextPath}/contact/edit" method="POST">
            
            <input type="hidden" name="id" value="${contact.id}" />
            First Name: <input placeholder="First Name" type="text"name="firstName" value="${contact.firstName}"/> <br/>
            Last Name: <input type="text" name="lastName" value="${contact.lastName}"/><br/>
            Company: <input type="text" name="company" value="${contact.company}"/><br/>
            email: <input type="text" name="email" value="${contact.email}"/><br/>
            phone: <input type="text" name="phone" value="${contact.phone}"/><br/>
            
            <input type="submit" value="Save" />


        </form>


    </body>
</html>
