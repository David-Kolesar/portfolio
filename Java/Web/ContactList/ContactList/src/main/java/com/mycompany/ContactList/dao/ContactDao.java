/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ContactList.dao;

import com.mycompany.ContactList.dto.Contact;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface ContactDao {
 
    public Contact create(Contact contact);
    
    public void delete(int contactId);
    
    public void update(Contact contact);
    
    public Contact read(int id);
    
    public List<Contact> all();
    
}
