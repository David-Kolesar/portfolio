/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ContactList.DaoImpl;

import com.mycompany.ContactList.dao.ContactDao;
import com.mycompany.ContactList.dto.Contact;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
/**
 *
 * @author apprentice
 */
public class ContactDaoImpl implements ContactDao {

    private static final String TOKEN = "::";
    public List<Contact> contactsList = null;
    public List<Contact> editList = null;
    private Integer nextID = 1;
    private String FILENAME = "Contacts.txt";

    public ContactDaoImpl() {

        String FILENAME = "Contacts.txt";

        contactsList = decode();

        for (Contact o : contactsList) {

            if (o.getId() == nextID) {
                nextID = o.getId() + 1;
            }
        }
    }
@Override
    public Contact create(Contact thisContact) {

        OptionalInt ID = contactsList.stream().mapToInt(o -> o.getId()).max(); //map to int only takes ints (gets the max ID) -- Do you need this?
        if (ID.isPresent()) {

            nextID = ID.getAsInt();

        } else {

            nextID = 0;

        }

        nextID++;

        thisContact.setId(nextID);

        contactsList.add(thisContact);
        
        encode();

        return thisContact;

    }

    public Contact read(int ID) {

        nextID = 1;

        for (Contact o : contactsList) {

            if (o.getId() == nextID) {
                return o;
            }
        }
        return null;

    }

    @Override
    public void update(Contact thisContact) {

        Contact toRemove = null;

        for (Contact c : contactsList) {

            if (c.getId() == thisContact.getId()) {
                toRemove = c;
            }

        }

        contactsList.remove(toRemove);
        contactsList.add(thisContact);
        encode();

    }

    @Override
    public void delete(int contactId) {

        for (Contact o : contactsList) {

            if (o.getId() == contactId) {
                contactsList.remove(o);
                break;
            }
        }

        encode();
    }

    private void newFile() {

        LocalDate newFile = LocalDate.now();
        String name = "Contacts.txt";
        File currentDateFileName = new File(name);

        if (!currentDateFileName.exists()) {

            try {
                currentDateFileName.createNewFile();

            } catch (IOException ex) {
                Logger.getLogger(ContactDaoImpl.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void encode() {

        newFile();

        PrintWriter out = null;

        try {

            out = new PrintWriter(new FileWriter(FILENAME));

            for (Contact c : contactsList) {
                out.print(c.getId());
                out.print(TOKEN);

                out.print(c.getFirstName());
                out.print(TOKEN);

                out.print(c.getLastName());
                out.print(TOKEN);

                out.print(c.getCompany());
                out.print(TOKEN);

                out.print(c.getEmail());
                out.print(TOKEN);

                out.print(c.getPhone());
                out.print(TOKEN);
                out.println("");

            }

            out.flush();

        } catch (IOException ex) {

        } finally {
            out.close();
        }

    }

    public List<Contact> decode() {

        List<Contact> tempContactList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME))); //   see if it needs this with updated token String[] stringParts = currentLine.split("(?<!\\\\)!!~~!!*"); //"?<!" is negative lookbehind if , does not follow \, you recognize it as a delimiter. It is Regex. 

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);

                Contact myContact = new Contact();

                int contactId = Integer.parseInt(stringParts[0]);
         
                myContact.setId(contactId);
                myContact.setFirstName(stringParts[1]);
                myContact.setLastName(stringParts[2]);
                myContact.setCompany(stringParts[3]);
                myContact.setEmail(stringParts[4]);
                myContact.setPhone(stringParts[5]);

                tempContactList.add(myContact);
            }

        } catch (FileNotFoundException ex) {
               Logger.getLogger(ContactDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tempContactList;
    }

    public List readAll() {

        List<Contact> tempContactList = new ArrayList();

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));

            while (sc.hasNextLine()) {

                String currentLine = sc.nextLine();

                String[] stringParts = currentLine.split(TOKEN);
                
                Contact thisContact = new Contact();

                int contactId = Integer.parseInt(stringParts[0]);

                thisContact.setId(contactId);
                thisContact.setFirstName(stringParts[1]); 
                thisContact.setCompany(stringParts[3]);
                thisContact.setEmail(stringParts[4]);
                thisContact.setPhone(stringParts[5]);

                tempContactList.add(thisContact);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ContactDao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return tempContactList;
    }

   

    public List<Contact> all() {

        return contactsList;

    }



}
