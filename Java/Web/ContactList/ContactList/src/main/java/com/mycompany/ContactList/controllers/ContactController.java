/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ContactList.controllers;

import com.mycompany.ContactList.dao.ContactDao;
import com.mycompany.ContactList.dto.Contact;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */

@Controller
@RequestMapping(value="/contact")
public class ContactController {
    
    private ContactDao contactDao;
    
    @Inject
    public ContactController(ContactDao contactDao){
        this.contactDao = contactDao;
        
    }
    
    @RequestMapping(value="show/{id}" , method=RequestMethod.GET)
    public String show(@PathVariable("id")int contactId, Map model) {
        
        
        Contact c = contactDao.read(contactId);
        model.put("contact", c);
        
        return "show";
        
        
    }
    
    
    @RequestMapping(value="create", method=RequestMethod.POST)
    public String create(@ModelAttribute Contact contact) {
        
        contactDao.create(contact);
        
     return "redirect:/";   
    }

    @RequestMapping(value="edit/{id}", method=RequestMethod.GET)
    public String edit(@PathVariable("id") Integer contactId, Map model){
        
        Contact c = contactDao.read(contactId);
        
        model.put("contact", c);
        return "edit";
        
    }

    
    @RequestMapping(value="edit", method=RequestMethod.POST)
    public String editSubmit(@ModelAttribute Contact contact) {
        
        contactDao.update(contact);
        
        return "redirect:/contact/show/" + contact.getId();
    }
    
    
    
    @RequestMapping(value="delete/{id}", method=RequestMethod.GET)
    public String delete(@PathVariable("id") Integer contactId){
        
        contactDao.delete(contactId);
        
       
        return "delete";

    
    }
    
    
    @RequestMapping(value="delete", method=RequestMethod.POST)
    public String deleteSubmit(@ModelAttribute Contact contact) {
        
        contactDao.update(contact);
        
        return "redirect:/contact/deleteShow/" + contact.getId();
    }
    
    
    
}
