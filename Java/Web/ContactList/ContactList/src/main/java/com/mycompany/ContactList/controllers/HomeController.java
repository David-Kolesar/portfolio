/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ContactList.controllers;

import com.mycompany.ContactList.dao.ContactDao;
import com.mycompany.ContactList.dto.Contact;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */

@Controller
public class HomeController {
  
    private ContactDao contactDao;
    
    @Inject

  
public HomeController(ContactDao contactDao){
    this.contactDao=contactDao;
}    
    
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Map model){

    List<Contact> contacts = contactDao.all();
        
    
    model.put("contactList", contacts);
    
    
    
        return "home";
        
    }
}
