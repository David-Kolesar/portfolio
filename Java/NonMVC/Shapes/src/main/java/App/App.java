/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.mycompany.consoleio.ConsoleIO;

/**
 *
 * @author David Kolesar
 */
public class App {

    public static void main(String[] args) {

        Shape square = new Square();
        Rectangle rectangle = new Rectangle();
        Triangle triangle = new Triangle();
        Circle circle = new Circle();
        
        square.Area();
        square.Perimeter();
        
        rectangle.Area();
        rectangle.Perimeter();
        
        triangle.Area();
        triangle.Perimeter();
        
        circle.Area();
        circle.Perimeter();
        
        

    }

}
