/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import App.Shape;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Triangle extends Shape {

    @Override
    public double Area() {

        String promptBase = "Please input the base";

        double base = getDouble(promptBase);

        String promptHeight = "Please input the height";

        double height = getDouble(promptHeight);

        double area = ((base * .5) * height);

        System.out.println(area);

        return area;

    }

    @Override
    public double Perimeter() {

        String promptA = "Please input the length of side A";

        double sideA = getDouble(promptA);

        String promptB = "Please input the length of side B";

        double sideB = getDouble(promptB);

        String promptC = "Please input the length of side C";

        double sideC = getDouble(promptC);

        double perimeter = sideA + sideB + sideC;

        System.out.println(perimeter);

        return perimeter;

    }

    public double getDouble(String prompt) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        double userDouble = 0;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();
                if (input.equals("NaN")) {
                    System.out.println("NaN is not a number");
                    String retry = ("Please enter a number.");
                    getDouble(retry);
                }
                userDouble = Double.parseDouble(input);

                valid = true;

            } catch (NumberFormatException e) {
                System.out.println("Enter a double: ");
            }

        }
        return userDouble;
    }

}
