/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import App.Shape;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Circle extends Shape {

    @Override
    public double Area() {

        double pi = 3.14159;
        
        String promptRadius = "Please input the Radius";

        double radius = getDouble(promptRadius);

        double area = pi * (radius * radius);
        
        System.out.println(area);
        
        return area;
        
    }

    @Override
    public double Perimeter() {
        double pi = 3.14159;

        String promptRadius = "Please input the Radius";

        double radius = getDouble(promptRadius);

        double perimeter = 2 * pi * radius;

        System.out.println(perimeter);
        
        return perimeter;
    }

    public double getDouble(String prompt) {

        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        double userDouble = 0;

        System.out.println(prompt);

        while (!valid) {
            try {
                String input = sc.nextLine();
                if (input.equals("NaN")) {
                    System.out.println("NaN is not a number");
                    String retry = ("Please enter a number.");
                    getDouble(retry);
                }
                userDouble = Double.parseDouble(input);

                valid = true;

            } catch (NumberFormatException e) {
                System.out.println("Enter a double: ");
            }

        }
        return userDouble;
    }

}
