/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;

/**
 *
 * @author David Kolesar
 */
public class primeFinder {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String inputString = "";
  

        System.out.println("Welcome to the Prime Number Machine!");
        System.out.println("");
        System.out.println("This program finds all the prime within a given range based off of an ancient algorithm known as \"The Sieve of Eratosthenes for Prime Numbers\" " );
        System.out.println("");
        System.out.println("The Sieve of Eratosthenes works by performing the following operations:");
        System.out.println("");
        System.out.println("First, the number 1 is discarded from the input range.");
        System.out.println("");
        System.out.println("This leaves 2 as the first number within any given range.");
        System.out.println("");
        System.out.println("Every other number within the range after 2 is discarded, as they are divisible by 2.");
        System.out.println("");
        System.out.println("Then, every third number within the range is discarded, as they are divisible by 3.");
        System.out.println("");
        System.out.println("Then, after the first instance of five, every fifth number is discarded from the input range.");
        System.out.println("");
        System.out.println("The remaining numbers are prime.");
        System.out.println("");
        System.out.println("Please enter your number here:");
        System.out.println("");
        inputString = sc.nextLine();
        int inputInt = Integer.parseInt(inputString);
        System.out.println("Oh, so you want to find all of the primes between " + inputInt + " and zero?");
        System.out.println("");

        // marks all integers as "prime"
        boolean truePrime[] = new boolean[inputInt + 1];
        // This also does us the favor of removing 1 from the list
        for (int i = 2; i <= inputInt; i++) {
            truePrime[i] = true;
        }

       // Then the program indicates which numbers are not prime (divisible by two) and <= inputInt using Sieve of Eratosthenes
       for (int i = 2; i * i <= inputInt; i++) {


           
       // if factor is prime, then mark multiples of factor as nonprime
       // This looks at mutiples factor, factor+1, ...,  n/factor
            for (int j = i; i * j <= inputInt; j++) {
                truePrime[i * j] = false;
            }
        }

       
       // Then we display the primes
        for (int i = 2; i <= inputInt; i++) {
            if (truePrime[i]) {
                System.out.println(i + " is a prime number.");
            }
        }
    }
}
